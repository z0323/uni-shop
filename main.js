
// #ifndef VUE3
import Vue from 'vue'
import App from './App'

import {$http} from "@escook/request-miniprogram"
uni.$http = $http
$http.baseUrl="https://api-hmugo-web.itheima.net"
// 网络请求拦截器
$http.bedoreRequest=function(options){
  uni.showLoading({
    title:'数据加载中...'
  })
}
// 请求完成之后做一些事情
$http.afterRequest = function () {
  wx.hideLoading()
}

uni.$showMsg=function(title="数据加载失败！",duration=1500){
  uni.showToast({
    title,
    duration,
    icon:"none"
  })
}

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif